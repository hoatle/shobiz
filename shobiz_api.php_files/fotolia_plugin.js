(function( window, undefined ) {
//GLOBAL OBJECT&VARIABLES//
//{
var document = window.document;
window.search = {
	Fotolia : {
		url : [
			"k={TEXT}",
			"order=relevance",
			"fotolia_x=0",
			"fotolia_y=0",
			"filters[content_type:photo]=1",
			"filters[orientation]=all",
			"filters[collection]=only_subscription",
			//"filters[isolated:on]=1",
			//"filters[license_L:on]=1",
			"limit=64"
		]
	},
	plugin : {
		global : {
			x : "",
			y : "",
			m : false,
			n : "",
			p : "",
			callbackFunction : "",
			customQuestion : "",
			hideonFinish : "",
			initQuery : "",
			pageLinks : "",
			reBrand : "",
			selectLimit : "",
			sentenceID : "",
			submitURL : "",
			totalResults : "",
			xPos : "",
			yPos : ""
		}
	}
};
var s = search.plugin,
	g = s.global;

//}
//PLUGIN FUNCTIONS//
//{
s.begin = function (o) {
	g.sentenceID = o.sentenceID;
	g.submitURL = (o.submitURL !== undefined)? o.submitURL : false;
	g.callbackFunction = (o.callbackFN !== undefined) ? o.callbackFN : false;
	g.selectLimit = o.numberToSelect;
	g.hideonFinish = (o.hideonFinish !== undefined) ? o.hideonFinish : true;
	g.customQuestion = (o.customQuestion !== undefined) ? o.customQuestion : "Please pick the top %numberToSelect% pictures that make you feel strongest.";
	g.reBrand = (o.reBrand !== undefined) ? o.reBrand : "Image Search";
	g.pageLinks = (o.pagelinks !== undefined) ? o.pagelinks : true;
	s.generate();
	if(o.showAtxPos !== undefined){
		g.xPos = o.showAtxPos;
		$("#fotolia_overlay_wrapper").css("left",o.showAtxPos);
	}
	else {
		g.xPos = "";
		$("#fotolia_overlay_wrapper").css({"left" : "50%", "margin-left" : "-404px"});
	}
	if(o.showAtyPos !== undefined){
		g.yPos = o.showAtyPos;
		$("#fotolia_overlay_wrapper").css("top",o.showAtyPos);
	}
	else {
		g.yPos = "";
		$("#fotolia_overlay_wrapper").css({"top" : "50%", "margin-top" : "-223px"});
	}
	if (!(jQuery().draggable)) {
		$.ajax({
			url : "http://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js",
			dataType : "script",
			success : function () {
				$("#fotolia_overlay_wrapper").draggable({handle : "#fotolia_title"});
			}
		});
	}
	else {
		$("#fotolia_overlay_wrapper").draggable({handle : "#fotolia_title"});
	}
	if(!(!o.initQuery||o.initQuery=="")) {
		$("#fotolia_input").attr("value",o.initQuery);
		$("#fotolia_input").css("color","#000000");
		s.handler(false,1);
	}
	else {
		$("#fotolia_input").css("color","#9E9E9E");
	}
};
window.changeImage = s.begin;
s.end = function () {
	$("#fotolia_overlay_wrapper_wrapper").remove();
};
s.generate = function () {
	s.end();
	$("body").append(
	'<div id="fotolia_overlay_wrapper_wrapper">'
		+'<table id="fotolia_overlay_wrapper" style="position:fixed">'
			+'<tr>'
				+'<td>'
					+'<div id="fotolia_overlay">'
						+'<div id="fotolia_title"> ' + g.reBrand + ' </div>'
						+'<a id="fotolia_close_button"  onclick="window.search.plugin.end();"></a>'
						+'<div id="fotolia_overlay_header">'
							+'<table id="fotolia_overlay_ui">'
								+'<tr class="fotolia_overlay_ui_col">'
									+'<td class="fotolia_overlay_ui_row_first">'
										+'<input id="fotolia_input" type="text" size="25" value="Search Tag(s)" onFocus="if(this.value==\'Search Tag(s)\') {this.value=\'\';this.style.color=\'#000000\'}" onBlur="if(this.value==\'\') {this.value=\'Search Tag(s)\';this.style.color=\'#9E9E9E\'}" />'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_last" style="width:80px">'
										+'<a id="fotolia_overlay_button" class="fotolia_overlay_button fotolia_overlay_button_right" onclick="window.search.plugin.handler(false, 1);">'
											+'<span class="fotolia_overlay_button_spn">Search</span>'
										+'</a>'
									+'</td>'								
								+'</tr>'
							+'</table>'
						+'</div>'
						+'<div id="fotolia_overlay_subheader">'
							+'<div>'
								+s.leader()
							+'</div>'
						+'</div>'
						+'<table id="fotolia_results_table">'
							+'<tr>'
								+'<td id="fotolia_results_wrapper">'
									+'<div id="fotolia_results_element">'
										+'&nbsp;'
									+'</div>'
								+'</td>'
							+'</tr>'
						+'</table>'
						+'<div id="fotolia_overlay_header">'
							+'<table id="fotolia_overlay_ui">'
								+'<tr class="fotolia_overlay_ui_col">'
									+'<td id="fotolia_options" class="fotolia_overlay_ui_row_first">&nbsp;'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_first" style="width:80px">'
										+'<a id="fotolia_overlay_button_pages_less"  class="fotolia_overlay_button fotolia_overlay_button_more fotolia_overlay_button_cancel fotolia_overlay_button_left" onclick="window.search.plugin.pagination(false);">'
											+'<span class="fotolia_overlay_button_spn fotolia_overlay_button_spn_noicon" style="color:transparent">&#9664;&#9664;</span>'
										+'</a>'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_first" style="width:80px">'
										+'<a id="fotolia_overlay_button_pages"  class="fotolia_overlay_button fotolia_overlay_button_more">'
											+'<span class="fotolia_overlay_button_spn fotolia_overlay_button_spn_noicon">Pages</span>'
										+'</a>'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_first" style="width:80px;">'
										+'<a id="fotolia_overlay_button_pages_more"  class="fotolia_overlay_button fotolia_overlay_button_more fotolia_overlay_button_cancel fotolia_overlay_button_right" onclick="window.search.plugin.pagination(true);">'
											+'<span class="fotolia_overlay_button_spn fotolia_overlay_button_spn_noicon" style="color:transparent">&#9658;&#9658;</span>'
										+'</a>'
									+'</td>'
									+'<td id="fotolia_options" class="fotolia_overlay_ui_row_first">&nbsp;'
									+'</td>'
								+'</tr>'
							+'</table>'
						+'</div>'
					+'</div>'
				+'</td>'
				+'<td>'
					+'<div id="fotolia_image_panel">'
						+'<div id="fotolia_image_title"> Chosen Images&nbsp;&nbsp;</div>'
						+'<a id="fotolia_maximize_button"  onclick="window.search.plugin.maximize();"></a>'
						+'<a id="fotolia_minimize_button"  onclick="window.search.plugin.minimize();"></a>'
						+'<table id="fotolia_image_table">'
							+'<tr>'
								+'<td id="fotolia_image_wrapper">'
									+'<div id="fotolia_image_element">'
										+'&nbsp;'
									+'</div>'
								+'</td>'
							+'</tr>'
						+'</table>'
						+'<div id="fotolia_overlay_header">'
							+'<table id="fotolia_overlay_ui">'
								+'<tr class="fotolia_overlay_ui_col">'
									+'<td id="fotolia_image_status" class="fotolia_overlay_ui_row_first">&nbsp;'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_first" style="display:none;width:80px">'
										+'<a id="fotolia_overlay_button_done_disabled" class="fotolia_overlay_button fotolia_overlay_button_single" onclick="window.search.plugin.submit();">'
											+'<span class="fotolia_overlay_button_spn fotolia_overlay_button_spn_noicon">Done</span>'
										+'</a>'
									+'</td>'
									+'<td class="fotolia_overlay_ui_row_last" style="width:80px">'
										+'<a id="fotolia_overlay_button_done" class="fotolia_overlay_button fotolia_overlay_button_cancel fotolia_overlay_button_single" onclick="window.search.plugin.submit();">'
											+'<span class="fotolia_overlay_button_spn fotolia_overlay_button_spn_noicon">Done</span>'
										+'</a>'
									+'</td>'
								+'</tr>'
							+'</table>'
						+'</div>'
					+'</div>'
				+'</td>'
			+'</tr>'
		+'</table>'
	+'</div>'
	);
};
s.leader = function () {
	var leaderstr =	g.customQuestion.replace(/\%sentenceID\%/g,g.sentenceID).replace(/\%submitURL\%/g,g.submitURL).replace(/\%callbackFN\%/g,g.callbackFunction).replace(/\%hideonFinish\%/g,g.hideonFinish).replace(/\%numberToSelect\%/,g.selectLimit).replace(/\%reBrand\%/,g.reBrand);
	return leaderstr;
};
//}
//EVENT PREP//
//{
$(document).ready(function () {
	$('#fotolia_input').live('keypress',function (e) {
		if(e.keyCode===13) {
			s.handler(false, 1);
			return false;
		}
	});
	$("#fotolia_results img").live("click",function () {
		s.select(this);
	});
}
);
s.handler = function (more,current) {
	g.m = more; g.n = current; g.p = current;
	s.fetch(g.x,g.y,more,current,current);
};
//}
//TABLE PREP//
//{
s.table = function (x,y,n) {
	g.x = x; g.y = y; g.n = n;g.p = n;
	var	trn="<tr value='" + n + "'></tr>",tdn="<td value='" + n + "'></td>";
	for (i = 1;i<y;i++) {trn+="<tr value='" + n + "'></tr>";}
	for (i = 1;i<x;i++) {tdn+="<td value='" + n + "'></td>";}
	if(n===1) {$("#fotolia_results_element").html('<table id="fotolia_results"></table>');}
	$("#fotolia_results").html($("#fotolia_results").html() + trn).find("tr[value='" + n + "']").html(tdn);
};
//}
//SEARCH FUNCTIONS//
//{
s.maximize = function () {
	$("#fotolia_maximize_button").css("display","none");
	$("#fotolia_minimize_button").css("display","block");
	$("#fotolia_overlay_wrapper").css({"left" : "0px", "top" : "0px", "margin-left" : "0px", "margin-top" :"0px"});
	s.maximize_handler();
	$(window).resize(function(){
		s.maximize_handler();
	});
};
s.maximize_handler = function(){
	var h = $(window).height(),
		w = $(window).width();
	$("#fotolia_results_element").css({height : (h-144)+"px", width : (w-199)+"px"});
	$("#fotolia_image_element").css({height : (h-85)+"px", width : "150px"});
};
s.minimize = function () {
	var x = g.xPos,
		y = g.yPos;
	$(window).unbind("resize");
	$("#fotolia_maximize_button").css("display","block");
	$("#fotolia_minimize_button").css("display","none");
	$("#fotolia_results_element").css({height : "300px", width : "610px"});
	$("#fotolia_image_element").css({height : "359px", width : "150px"});
	if(x !== ""){
		$("#fotolia_overlay_wrapper").css("left",x);
	}
	else {
		$("#fotolia_overlay_wrapper").css({
			"left" : "50%",
			"margin-left" : "-404px"
		});
	}
	if(y !== ""){
		$("#fotolia_overlay_wrapper").css("top",y);
	}
	else {
		$("#fotolia_overlay_wrapper").css({
			"top" : "50%",
			"margin-top" : "-223px"
		});
	}
};
s.fetch = function (x,y,m,n,p) {
	s.table(5,13,(m===false)?1:n);
	$("#fotolia_results td[value='" + n + "']").first().html('<div id="fotolia_loader_img"></div>');
	s.display();

	var offset = 0;
	if (m != false){
		offset = (n-1)*64;
	}

	params = {
		        "query" : $("#fotolia_input").attr("value"),
				"offset" : 0
		    };
		    
	$.ajax({
		//url : "http://query.yahooapis.com/v1/public/yql/mcbryanmd/fotolia",
        url : "/fotolia/",
		//dataType : "jsonp",
		data : params,
		success : function(data){
			search.Fotolia.success($.parseJSON(data),g.n);
		}
	});
};
s.pagination = function (mode) {
	var n = g.n,
		p = g.p,
		currentlimit = (p  * 64) + 1,
		results = g.totalResults*1;
	if (mode) {
		if(p === n && currentlimit < results){
			p++;
			s.handler(true, p);
		}
		else if(p < n){
			p++;
			s.display(p);
		}
	}
	else {
		if(p > 1){
			p--;
			s.display(p);
		}
	}
	if(p > 1 && n === p){
		$("#fotolia_overlay_button_pages_less span").css("color","#000000");
		$("#fotolia_overlay_button_pages_more span").css("color",(currentlimit < (results - 64)) ? "#000000" : "transparent");
	}
	else if(p > 1 && p < n){
		$("#fotolia_overlay_button_pages_less span, #fotolia_overlay_button_pages_more span").css("color","#000000");
	}
	else if(p === 1 && p < n){
		$("#fotolia_overlay_button_pages_less span").css("color","transparent");
		$("#fotolia_overlay_button_pages_more span").css("color","#000000");
	}
	else if(p === 1 && n === p){
		$("#fotolia_overlay_button_pages_less span, #fotolia_overlay_button_pages_more span").css("color","transparent");
	}
};
s.display = function(t){
	if(t !== undefined){g.p=t;}
	var n = g.n, p = g.p;
	$("#fotolia_results tr").css("display","none");
	$("#fotolia_results tr[value='" + p + "']").css("display","");
	$("#fotolia_overlay_button_pages span").html(($("#fotolia_results tr[value='" + p + "']").first().find("img").attr("imgid")) ? ((((p - 1) * 64) + 1) + "-" + (((p - 1) * 64) + $("#fotolia_results tr[value='" + p + "'] img").size()) + " of " + g.totalResults) : "Loading");
};
s.select = function (selection) {
	if (($("#fotolia_image_element td[deselector!='true'] img").size() < g.selectLimit) && ($(selection).attr("class") !== "imageresult-selected")) {
		var selectedimg = $(selection).clone();
		$(selection).attr("class","imageresult-selected");
		if ($("#fotolia_image_element img").size() === 0) {
			$("#fotolia_image_element").html('<table id="fotolia_image_chosen"></table>');	
		}
		$("#fotolia_image_chosen").append(
			'<tr>'
				+'<td deselector="true">'
					+'<div class="fotolia_delete_button"  onclick="window.search.plugin.deselect(this);"></div>'
				+'</td>'
				+'<td></td>'
			+'</tr>'
		);
		if(g.pageLinks !== false){
			$("#fotolia_image_chosen td[deselector]").last().append('<a class="fotolia_pagelinks_button" href="http://us.fotolia.com/id/' + $(selectedimg).attr("imgid") +'" target="_blank"></a>');
		}
		$("#fotolia_image_chosen td").last().append(selectedimg);
		s.monitor();
	}
};
s.deselect = function (selection) {
	$(selection).parent().parent().find("img").last().each(function(){
		$("#fotolia_results img[imgid='" + $(this).attr("imgid") + "']").attr("class","imageresult");
	});
	$(selection).parent().parent().remove();
	s.monitor();
};
s.monitor = function(){
	var mon = ($("#fotolia_image_element td[deselector!='true'] img").size() === g.selectLimit)?true:false;
	$("#fotolia_overlay_button_done_disabled").parent().css("display",(mon) ? "" : "none");
	$("#fotolia_overlay_button_done").parent().css("display",(mon) ? "none" : "");
};
s.submit = function(){
	var chosen = [], tsID = g.sentenceID;
	if (g.sentenceID == "ASSIGNMENT_ID_NOT_AVAILABLE") {
		$("#fotolia_image_status").html("ER:!ID");
		return;
	}
	$("#fotolia_image_chosen img[imgid]").each(function(index){
		chosen[index] = '{"id":"' + $(this).attr("imgid") + '","image":"' + $(this).attr("src") + '"}';
	});
	if ($(chosen).size() !== g.selectLimit){
		$("#fotolia_image_status").html("ER:!LIM");
		return;	
	}
	toSubmit = chosen;
	
	chosen = "#" + encodeURIComponent("[" + chosen.join(",") + "]");
	if (g.submitURL == false){
		$("#fotolia_image_status").html("Success!");
		if (window[g.callbackFunction] !== undefined) {
			window[g.callbackFunction](g.sentenceID, toSubmit);
		}
		
		if(g.hideonFinish === true) {
			s.end();
		}
	}else if(g.callbackFunction !== false) {
		$.ajax({
			type: "POST",
			url: g.submitURL,
			data: { sentenceID : tsID, images : chosen},
			success: function(data) {
				$("#fotolia_image_status").html("Success!");
				if (window[g.callbackFunction] !== undefined) {
					window[g.callbackFunction](g.sentenceID);
				}
				if(g.hideonFinish === true) {
					s.end();
				}
			}
		});
	}
	else {
		var tempForm = $("<form></form>",{
			action : g.submitURL,
			method : "POST",
			id : "fotolia_selection_form"
		}),
			tempID = $("<input />",{
				id : "assignmentId",
				name : "assignmentId",
				value : tsID
			}),
			tempSE = $("<input />",{
				id : "select",
				name : "select",
				value : chosen
			});
		$(tempForm).appendTo("#fotolia_overlay_wrapper_wrapper");
		$(tempID).appendTo("#fotolia_selection_form");
		$(tempSE).appendTo("#fotolia_selection_form");
		document.getElementById("fotolia_selection_form").submit();
	}
};
//}
//PROFILES//
//{
//PROFILE -- Fotolia
//{
search.Fotolia.success = function (data,n) {
	if(data.error === undefined && data.query !== undefined) {
		if(data.query.count !== 0){
			var imgarray = [];
			var nresults = data.query.count;
			data = data.query.results;
			if(n === 1){
				 //data.h2.replace(/( Search results: | files| file)/g,"");
				g.totalResults = nresults;
				$("#fotolia_overlay_button_pages_more span").css("color",((nresults*1)<=64) ? "transparent" : "#000000");
				$("#fotolia_overlay_button_pages_less span").css("color","transparent");
				if((nresults*1)===0){
					$("#fotolia_results,#fotolia_overlay_button_pages span").html("No Results.");
					return;
				}
				else{
					$("#fotolia_results").before(
						"<span>Search for \"" + $("#fotolia_input").attr("value") + "\" returned " + nresults + " results.</span>"
					);
				}
			}
			else {	
				$("#fotolia_overlay_button_pages_less span").css("color","#000000");
			}
			if (data.a[0] === undefined) {data.a[0] = data.a;alert("1");}
			for (i = 0;i<$(data.a).size();i++) {
				imgarray[i]= $("<img/>",{
					"class" : "imageresult",
					src : data.a[i].img.src,
					lrgsrc : data.a[i].img.src.replace(/\/110_/,"/400_"),
					alt : data.a[i].img.alt,
					title : data.a[i].img.alt,
					style : "height:" + data.a[i].img.height + "px;width:" + data.a[i].img.width + "px;cursor:pointer;",
					imgid : data.a[i].href.replace(/\/id\//,"")
				});
			}
			$("#fotolia_results td[value='" + n + "']").each(function (index) {
				if(imgarray[index] === undefined) { return false; }
				$(this).empty().append(imgarray[index]);
			});
			s.display();
		}
		else {
			$("#fotolia_results,#fotolia_overlay_button_pages span").html("No Results.");
			$("#fotolia_overlay_button_pages span").html("Pages");
		}
	}
	else {
		$("#fotolia_results,#fotolia_overlay_button_pages span").html(data.error.description);
		$("#fotolia_overlay_button_pages span").html("Error");
	}
};
//}
//}
//CLIENT-SPECIFIC FUNCTIONS//
//{
//}
// DIAGNOSTICS
//{
function dump(arr,level) {
	var dumped_text = "";
	if(!level) {level = 0;}
	
	//The padding given at the beginning of the line.
	var level_padding = "";
	for(var j=0;j<level+1;j++) level_padding += "    ";
	
	if(typeof(arr) == 'object') { //Array/Hashes/Objects 
		for(var item in arr) {
			var value = arr[item];
			
			if(typeof(value) == 'object') { //If it is an array,
				dumped_text += level_padding + "'" + item + "' ...\n";
				dumped_text += dump(value,level+1);
			} else {
				dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
			}
		}
	} else { //Stings/Chars/Numbers etc.
		dumped_text = "===>"+arr+"<===("+typeof(arr)+")";
	}
	return dumped_text;
}
//}
})(window);
