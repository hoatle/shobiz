var $_, $hxClasses = $hxClasses || {}, $estr = function () {
  return js.Boot.__string_rec(this, '');
}
function $extend(from, fields) {
  function inherit() {
  }

  ;
  inherit.prototype = from;
  var proto = new inherit();
  for (var name in fields) proto[name] = fields[name];
  return proto;
}
var Hash = $hxClasses["Hash"] = function () {
  this.h = { };
};
Hash.__name__ = ["Hash"];
Hash.prototype = {
  h: null, set: function (key, value) {
    this.h["$" + key] = value;
  }, get: function (key) {
    return this.h["$" + key];
  }, exists: function (key) {
    return this.h.hasOwnProperty("$" + key);
  }, remove: function (key) {
    key = "$" + key;
    if (!this.h.hasOwnProperty(key)) return false;
    delete(this.h[key]);
    return true;
  }, keys: function () {
    var a = [];
    for (var key in this.h) {
      if (this.h.hasOwnProperty(key)) a.push(key.substr(1));
    }
    return a.iterator();
  }, iterator: function () {
    return { ref: this.h, it: this.keys(), hasNext: function () {
      return this.it.hasNext();
    }, next: function () {
      var i = this.it.next();
      return this.ref["$" + i];
    }};
  }, toString: function () {
    var s = new StringBuf();
    s.b[s.b.length] = "{";
    var it = this.keys();
    while (it.hasNext()) {
      var i = it.next();
      s.b[s.b.length] = i == null ? "null" : i;
      s.b[s.b.length] = " => ";
      s.add(Std.string(this.get(i)));
      if (it.hasNext()) s.b[s.b.length] = ", ";
    }
    s.b[s.b.length] = "}";
    return s.b.join("");
  }, __class__: Hash
}
var IntIter = $hxClasses["IntIter"] = function (min, max) {
  this.min = min;
  this.max = max;
};
IntIter.__name__ = ["IntIter"];
IntIter.prototype = {
  min: null, max: null, hasNext: function () {
    return this.min < this.max;
  }, next: function () {
    return this.min++;
  }, __class__: IntIter
}

var PhraseSuggest = $hxClasses["PhraseSuggest"] = function () {
};
PhraseSuggest.__name__ = ["PhraseSuggest"];
PhraseSuggest.prototype = {
  index: null, res: null, addMatch: function (rs) {
    if (this.index.get(rs) == null) {
      this.res.push(rs);
      this.index.set(rs, "true");
    } else {
    }
  }, addwordsLeft: function (rs, numWordsBeyond, beforeary) {
    var pre = [rs];
    var j = beforeary.length;
    while (j-- > 0) {
      if (beforeary[j] != null && beforeary[j] != "") {
        pre.unshift(beforeary[j]);
        this.addMatch(pre.join(" "));
      }
      if (pre.length > numWordsBeyond) break;
    }
  }, addwordsRight: function (rs, numWordsBeyond, afterarray) {
    var pre = [rs];
    var _g1 = 0, _g = afterarray.length;
    while (_g1 < _g) {
      var j = _g1++;
      if (afterarray[j] != null && afterarray[j] != "") {
        pre.push(afterarray[j]);
        this.addMatch(pre.join(" "));
      }
      if (pre.length > numWordsBeyond) break;
    }
  }, addwordsRightAndLeftofIndex: function (rs, numWordsBeyond, beforeary, afterarray) {
    var pre = [rs];
    var j = beforeary.length - 1;
    var k = 0;
    while (true) {
      if (beforeary[j] != null && beforeary[j] != "") pre.unshift(beforeary[j]);
      if (afterarray[k] != null && afterarray[k] != "") pre.push(afterarray[k]);
      this.addMatch(pre.join(" "));
      if (pre.length > numWordsBeyond) break;
      j--;
      k++;
    }
  }, getMatches: function (s, input, numWordsBeyond) {
    var currentCharIndex = 0;
    this.index = new Hash();
    this.res = new Array();
    if (input.length > 0) while (true) {
      currentCharIndex = s.indexOf(input, currentCharIndex);
      if (currentCharIndex == -1) break;
      var A = currentCharIndex;
      var B = currentCharIndex + input.length - 1;
      var charsLeft = 0;
      var charsRight = 0;
      var j = currentCharIndex;
      while (j-- > 0) {
        var cc = s.charCodeAt(j);
        if (cc > 32 && cc != 160) {
          A = j;
          charsLeft++;
        } else {
          A = j + 1;
          break;
        }
      }
      var _g1 = 0, _g = input.length;
      while (_g1 < _g) {
        var m = _g1++;
        currentCharIndex++;
      }
      var _g1 = currentCharIndex, _g = s.length;
      while (_g1 < _g) {
        var k = _g1++;
        var ccc = s.charCodeAt(k);
        if (ccc > 32 && ccc != 160) {
          B = k + 1;
          charsRight++;
        } else {
          B = k;
          break;
        }
      }
      var before = s.substr(0, A);
      var rs = s.substr(A, B - A);
      var after = s.substr(B, s.length - B);
      var beforeAry = before.split(" ");
      var afterAry = after.split(" ");
      if (charsLeft == 0 && charsRight == 0) {
        this.addMatch(rs);
        this.addwordsLeft(rs, 1, beforeAry);
        this.addwordsRight(rs, 1, afterAry);
        this.addwordsRightAndLeftofIndex(rs, 1, beforeAry, afterAry);
      } else {
        this.addMatch(rs);
        this.addwordsLeft(rs, 1, beforeAry);
        this.addwordsRight(rs, 1, afterAry);
        this.addwordsRightAndLeftofIndex(rs, 1, beforeAry, afterAry);
      }
    }
  }, __class__: PhraseSuggest
}
var Std = $hxClasses["Std"] = function () {
}
Std.__name__ = ["Std"];
Std["is"] = function (v, t) {
  return js.Boot.__instanceof(v, t);
}
Std.string = function (s) {
  return js.Boot.__string_rec(s, "");
}
Std["int"] = function (x) {
  return x | 0;
}
Std.parseInt = function (x) {
  var v = parseInt(x, 10);
  if (v == 0 && x.charCodeAt(1) == 120) v = parseInt(x);
  if (isNaN(v)) return null;
  return v;
}
Std.parseFloat = function (x) {
  return parseFloat(x);
}
Std.random = function (x) {
  return Math.floor(Math.random() * x);
}
Std.prototype = {
  __class__: Std
}
var StringBuf = $hxClasses["StringBuf"] = function () {
  this.b = new Array();
};
StringBuf.__name__ = ["StringBuf"];
StringBuf.prototype = {
  add: function (x) {
    this.b[this.b.length] = x == null ? "null" : x;
  }, addSub: function (s, pos, len) {
    this.b[this.b.length] = s.substr(pos, len);
  }, addChar: function (c) {
    this.b[this.b.length] = String.fromCharCode(c);
  }, toString: function () {
    return this.b.join("");
  }, b: null, __class__: StringBuf
}
var haxe = haxe || {}
haxe.Log = $hxClasses["haxe.Log"] = function () {
}
haxe.Log.__name__ = ["haxe", "Log"];
haxe.Log.trace = function (v, infos) {
  js.Boot.__trace(v, infos);
}
haxe.Log.clear = function () {
  js.Boot.__clear_trace();
}
haxe.Log.prototype = {
  __class__: haxe.Log
}
var js = js || {}
js.Boot = $hxClasses["js.Boot"] = function () {
}
js.Boot.__name__ = ["js", "Boot"];
js.Boot.__unhtml = function (s) {
  return s.split("&").join("&amp;").split("<").join("&lt;").split(">").join("&gt;");
}
js.Boot.__trace = function (v, i) {
  var msg = i != null ? i.fileName + ":" + i.lineNumber + ": " : "";
  msg += js.Boot.__string_rec(v, "");
  var d = document.getElementById("haxe:trace");
  if (d != null) d.innerHTML += js.Boot.__unhtml(msg) + "<br/>"; else if (typeof(console) != "undefined" && console.log != null) console.log(msg);
}
js.Boot.__clear_trace = function () {
  var d = document.getElementById("haxe:trace");
  if (d != null) d.innerHTML = "";
}
js.Boot.__string_rec = function (o, s) {
  if (o == null) return "null";
  if (s.length >= 5) return "<...>";
  var t = typeof(o);
  if (t == "function" && (o.__name__ != null || o.__ename__ != null)) t = "object";
  switch (t) {
    case "object":
      if (o instanceof Array) {
        if (o.__enum__ != null) {
          if (o.length == 2) return o[0];
          var str = o[0] + "(";
          s += "\t";
          var _g1 = 2, _g = o.length;
          while (_g1 < _g) {
            var i = _g1++;
            if (i != 2) str += "," + js.Boot.__string_rec(o[i], s); else str += js.Boot.__string_rec(o[i], s);
          }
          return str + ")";
        }
        var l = o.length;
        var i;
        var str = "[";
        s += "\t";
        var _g = 0;
        while (_g < l) {
          var i1 = _g++;
          str += (i1 > 0 ? "," : "") + js.Boot.__string_rec(o[i1], s);
        }
        str += "]";
        return str;
      }
      var tostr;
      try {
        tostr = o.toString;
      } catch (e) {
        return "???";
      }
      if (tostr != null && tostr != Object.toString) {
        var s2 = o.toString();
        if (s2 != "[object Object]") return s2;
      }
      var k = null;
      var str = "{\n";
      s += "\t";
      var hasp = o.hasOwnProperty != null;
      for (var k in o) {
        ;
        if (hasp && !o.hasOwnProperty(k)) {
          continue;
        }
        if (k == "prototype" || k == "__class__" || k == "__super__" || k == "__interfaces__" || k == "__properties__") {
          continue;
        }
        if (str.length != 2) str += ", \n";
        str += s + k + " : " + js.Boot.__string_rec(o[k], s);
      }
      s = s.substring(1);
      str += "\n" + s + "}";
      return str;
    case "function":
      return "<function>";
    case "string":
      return o;
    default:
      return String(o);
  }
}
js.Boot.__interfLoop = function (cc, cl) {
  if (cc == null) return false;
  if (cc == cl) return true;
  var intf = cc.__interfaces__;
  if (intf != null) {
    var _g1 = 0, _g = intf.length;
    while (_g1 < _g) {
      var i = _g1++;
      var i1 = intf[i];
      if (i1 == cl || js.Boot.__interfLoop(i1, cl)) return true;
    }
  }
  return js.Boot.__interfLoop(cc.__super__, cl);
}
js.Boot.__instanceof = function (o, cl) {
  try {
    if (o instanceof cl) {
      if (cl == Array) return o.__enum__ == null;
      return true;
    }
    if (js.Boot.__interfLoop(o.__class__, cl)) return true;
  } catch (e) {
    if (cl == null) return false;
  }
  switch (cl) {
    case Int:
      return Math.ceil(o % 2147483648.0) === o;
    case Float:
      return typeof(o) == "number";
    case Bool:
      return o === true || o === false;
    case String:
      return typeof(o) == "string";
    case Dynamic:
      return true;
    default:
      if (o == null) return false;
      return o.__enum__ == cl || cl == Class && o.__name__ != null || cl == Enum && o.__ename__ != null;
  }
}
js.Boot.__init = function () {
  js.Lib.isIE = typeof document != 'undefined' && document.all != null && typeof window != 'undefined' && window.opera == null;
  js.Lib.isOpera = typeof window != 'undefined' && window.opera != null;
  Array.prototype.copy = Array.prototype.slice;
  Array.prototype.insert = function (i, x) {
    this.splice(i, 0, x);
  };
  Array.prototype.remove = Array.prototype.indexOf ? function (obj) {
    var idx = this.indexOf(obj);
    if (idx == -1) return false;
    this.splice(idx, 1);
    return true;
  } : function (obj) {
    var i = 0;
    var l = this.length;
    while (i < l) {
      if (this[i] == obj) {
        this.splice(i, 1);
        return true;
      }
      i++;
    }
    return false;
  };
  Array.prototype.iterator = function () {
    return { cur: 0, arr: this, hasNext: function () {
      return this.cur < this.arr.length;
    }, next: function () {
      return this.arr[this.cur++];
    }};
  };
  if (String.prototype.cca == null) String.prototype.cca = String.prototype.charCodeAt;
  String.prototype.charCodeAt = function (i) {
    var x = this.cca(i);
    if (x != x) return undefined;
    return x;
  };
  var oldsub = String.prototype.substr;
  String.prototype.substr = function (pos, len) {
    if (pos != null && pos != 0 && len != null && len < 0) return "";
    if (len == null) len = this.length;
    if (pos < 0) {
      pos = this.length + pos;
      if (pos < 0) pos = 0;
    } else if (len < 0) len = this.length + len - pos;
    return oldsub.apply(this, [pos, len]);
  };
  Function.prototype["$bind"] = function (o) {
    var f = function () {
      return f.method.apply(f.scope, arguments);
    };
    f.scope = o;
    f.method = this;
    return f;
  };
}
js.Boot.prototype = {
  __class__: js.Boot
}
js.Lib = $hxClasses["js.Lib"] = function () {
}
js.Lib.__name__ = ["js", "Lib"];
js.Lib.isIE = null;
js.Lib.isOpera = null;
js.Lib.document = null;
js.Lib.window = null;
js.Lib.alert = function (v) {
  alert(js.Boot.__string_rec(v, ""));
}
js.Lib.eval = function (code) {
  return eval(code);
}
js.Lib.setErrorHandler = function (f) {
  js.Lib.onerror = f;
}
js.Lib.prototype = {
  __class__: js.Lib
}
js.Boot.__res = {}
js.Boot.__init();
{
  Math.__name__ = ["Math"];
  Math.NaN = Number["NaN"];
  Math.NEGATIVE_INFINITY = Number["NEGATIVE_INFINITY"];
  Math.POSITIVE_INFINITY = Number["POSITIVE_INFINITY"];
  $hxClasses["Math"] = Math;
  Math.isFinite = function (i) {
    return isFinite(i);
  };
  Math.isNaN = function (i) {
    return isNaN(i);
  };
}
{
  String.prototype.__class__ = $hxClasses["String"] = String;
  String.__name__ = ["String"];
  Array.prototype.__class__ = $hxClasses["Array"] = Array;
  Array.__name__ = ["Array"];
  var Int = $hxClasses["Int"] = { __name__: ["Int"]};
  var Dynamic = $hxClasses["Dynamic"] = { __name__: ["Dynamic"]};
  var Float = $hxClasses["Float"] = Number;
  Float.__name__ = ["Float"];
  var Bool = $hxClasses["Bool"] = Boolean;
  Bool.__ename__ = ["Bool"];
  var Class = $hxClasses["Class"] = { __name__: ["Class"]};
  var Enum = { };
  var Void = $hxClasses["Void"] = { __ename__: ["Void"]};
}
{
  if (typeof document != "undefined") js.Lib.document = document;
  if (typeof window != "undefined") {
    js.Lib.window = window;
    js.Lib.window.onerror = function (msg, url, line) {
      var f = js.Lib.onerror;
      if (f == null) return false;
      return f(msg, [url + ":" + line]);
    };
  }
}
js.Lib.onerror = null;