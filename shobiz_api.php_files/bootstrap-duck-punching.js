/**
 * Duck-punching for bootstrap
 */

!(function ($) {

  var oldPopover = $.fn.popover;

  //duck punching
  $.fn.popover = function (option) {
    return this.each(function () {
      var $this = $(this)
        , data = $this.data('popover')
        , options = typeof option == 'object' && option
      if (!data) {
        $this.data('popover', (data = new $.fn.popover.Constructor(this, options)));
      }
      if (typeof option == 'string') {
        data[option]();
      }
    })
  }

  $.fn.popover.Constructor = function (element, options) {
    if (options.closeButton) {
      options.template = '<div class="popover"><div class="arrow"></div><button class="close" data-dismiss="alert">×</button><div class="popover-inner"><h3 class="popover-title"></h3><div class="popover-content"><p></p></div></div></div>';
    }

    if (options.placement === 'auto') {

      $(window).on('resize', $.proxy(function () {
        this.resize();
      }, this));


      options.placement = function () {
        var $element = this.$element,
          $tip = this.$tip;
        var topHeight, rightWidth, bottomHeight, leftWidth;

        var elementOffset = $element.offset();
        topHeight = elementOffset.top - $(window).scrollTop();
        leftWidth = elementOffset.left;
        rightWidth = $(window).width() - leftWidth - $element.outerWidth(true);
        bottomHeight = $(window).height() - topHeight - $element.outerWidth(true);

        var $popoverInner = $tip.find('.popover-inner');

        var popoverWidth = 0,
          popoverHeight = 0;

        if (this.options.maxWidth > 0) {
          popoverWidth = Math.min(this.options.$container.outerWidth(), this.options.maxWidth);
          $popoverInner.width(popoverWidth);
        }

        if (this.options.maxHeight > 0) {
          popoverHeight = this.options.maxHeight;
          $tip.find('.popover-content').css({
            'max-height': popoverHeight
          });
        }

        if (rightWidth < popoverWidth) {
          if (topHeight < bottomHeight) {
            return 'bottom';
          } else {
            return 'top';
          }
        } else {
          return 'right';
        }

      }
    }

    this.init('popover', element, options);
  };

  //extends popover for responsive
  $.fn.popover.Constructor.prototype = $.extend({}, oldPopover.Constructor.prototype, {
    shown: function () {
      var $tip = this.tip();
      return $tip.is(':visible');
    },
    //to avoid override, should find way to have super.show()
    //show only if it's not yet visible
    smartShow: function () {
      if (!this.shown()) {
        this.show();
      }
    },

    resize: function () {

      if (this.shown()) {
        var $tip
          , inside
          , pos
          , actualWidth
          , actualHeight
          , placement
          , tp;


        $tip = this.tip();

        placement = typeof this.options.placement == 'function' ?
          this.options.placement.call(this, $tip[0], this.$element[0]) :
          this.options.placement

        inside = /in/.test(placement)

        pos = this.getPosition(inside)

        actualWidth = $tip[0].offsetWidth
        actualHeight = $tip[0].offsetHeight

        switch (inside ? placement.split(' ')[1] : placement) {
          case 'bottom':
            tp = {top: pos.top + pos.height, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'top':
            tp = {top: pos.top - actualHeight, left: pos.left + pos.width / 2 - actualWidth / 2}
            break
          case 'left':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left - actualWidth}
            break
          case 'right':
            tp = {top: pos.top + pos.height / 2 - actualHeight / 2, left: pos.left + pos.width}
            break
        }

        $tip.removeClass('top').removeClass('right').removeClass('bottom').removeClass('left');

        $tip
          .css(tp)
          .addClass(placement);
      }

    }
  });

  //override bootstrap popover template
  $.fn.popover.defaults = $.extend({}, oldPopover.defaults, {
    closeButton: false,
    maxWidth: 0, //auto
    maxHeight: 0 //auto
  });

})(jQuery);