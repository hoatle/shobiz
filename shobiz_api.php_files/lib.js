function replace(str){
	str = str.replace(/\u2019/g,"'");   
	str = str.replace(/\u2026/g,"...");
	str = str.replace(/\u00B4/g,"'");
	str = str.replace(/\u2012/g,"-");
	str = str.replace(/\u2013/g,"-");
	str = str.replace(/\u2014/g,"-");
	str = str.replace(/\u2015/g,"--");
	
	str = str.replace(/\u201C/g,'"');
	str = str.replace(/\u201D/g,'"');
	
	return str;
}